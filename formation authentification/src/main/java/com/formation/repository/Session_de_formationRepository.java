package com.formation.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.formation.entities.Session_de_formation;

@Repository
public interface Session_de_formationRepository extends JpaRepository<Session_de_formation, Long>  {
	
}

package com.formation.service;


import com.formation.entities.Formation;
import com.formation.response.MessageResponse;

import java.util.List;
public interface FormationService {
	
	public MessageResponse save(Formation formation); // méthode ajouter 
    public MessageResponse update(Formation formation);
    public MessageResponse delete(Long id);
    public List<Formation> findAll();
    public Formation findById(Long id);

}

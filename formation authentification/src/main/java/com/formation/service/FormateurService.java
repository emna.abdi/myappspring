package com.formation.service;

import java.util.List;

import com.formation.entities.Formateur;
import com.formation.response.MessageResponse;

public interface FormateurService {

	  public MessageResponse save(Formateur formateur); // méthode ajouter 
	    public MessageResponse update(Formateur formateur);
	    public MessageResponse delete(Long IdFormateur);
	    public List<Formateur> findAll();
	    public Formateur findById(Long IdFormateur);
}

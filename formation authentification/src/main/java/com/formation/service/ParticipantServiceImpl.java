package com.formation.service;


import com.formation.entities.Organisme;
import com.formation.entities.Participant;
import com.formation.response.MessageResponse;
import com.formation.repository.ParticipantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ParticipantServiceImpl  implements ParticipantService {

@Autowired
ParticipantRepository participantRepository;

    @Transactional
    @Override
    public MessageResponse save(Participant participant) {
    	
    	// test si participant exsite déja ou non 
        boolean existe = participantRepository.existsByEmail(participant.getEmail());
        if (existe){
            return new MessageResponse ("Cette participant existe déja !");
        }
        participantRepository.save(participant);
        return new MessageResponse("Opération réalisée avec succès.");
    }


    @Transactional
    @Override
    public MessageResponse update(Participant participant) {
   
    	boolean existe = participantRepository.existsById(participant.getId());
        if (!existe){
        	boolean existe1 =participantRepository.existsByEmail(participant.getEmail());
            return new MessageResponse ("Cette participant existe déja !");
        }
        participantRepository.save(participant);
        return new MessageResponse("Opération réalisée avec succès.");
    
    }

    @Transactional
    @Override
    public MessageResponse delete(Long id) {
        Participant participant = findById(id);
        if (participant==null){
            return new MessageResponse("Cet enregistrement n'existe pas !");
        }
        participantRepository.delete(participant);
        return new MessageResponse( "L'enregistrement à été supprimé avec succès.");
    }

    @Override
    public List<Participant> findAll() {

        return participantRepository.findAll();
    }

    @Override
    public Participant findById(Long id) {
        Participant participant = participantRepository.findById(id).orElse(null);
        return participant;
    }
}


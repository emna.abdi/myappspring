package com.formation.service;

import java.util.List;

import com.formation.entities.Domaine;
import com.formation.response.MessageResponse;


public interface DomaineService {
    public MessageResponse save(Domaine domaine);
    public MessageResponse update(Domaine domaine);
    public MessageResponse delete(Long id);
    public List<Domaine> findAll();
    public Domaine findById(Long id);
}

package com.formation.service;

import com.formation.entities.Formation;
import com.formation.entities.Organisme;
import com.formation.response.MessageResponse;
import com.formation.repository.OrganismeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OrganismeServiceImpl  implements OrganismeService {

@Autowired
OrganismeRepository organismeRepository;

    @Transactional
    @Override
    public MessageResponse save(Organisme organisme) {
    	
    	// test si Organisme exsite déja ou non 
        boolean existe = organismeRepository.existsByLibelle(organisme.getLibelle());
        if (existe){
            return new MessageResponse ("Cette organisme existe déja !");
        }
        organismeRepository.save(organisme);
        return new MessageResponse("Opération réalisée avec succès.");
    }


    @Transactional
    @Override
    public MessageResponse update(Organisme organisme) {
    	boolean existe = organismeRepository.existsById(organisme.getId());
        if (!existe){
        	boolean existe1 =organismeRepository.existsByLibelle(organisme.getLibelle());
            return new MessageResponse ("Cette organisme existe déja !");
        }
        organismeRepository.save(organisme);
        return new MessageResponse("Opération réalisée avec succès.");
    }

    @Transactional
    @Override
    public MessageResponse delete(Long id) {
        Organisme organisme = findById(id);
        if (organisme==null){
            return new MessageResponse("Cet enregistrement n'existe pas !");
        }
        organismeRepository.delete(organisme);
        return new MessageResponse("L'enregistrement à été supprimé avec succès.");
    }

    @Override
    public List<Organisme> findAll() {

        return organismeRepository.findAll();
    }

    @Override
    public Organisme findById(Long id) {
        Organisme organisme =organismeRepository.findById(id).orElse(null);
        return organisme;
    }
}


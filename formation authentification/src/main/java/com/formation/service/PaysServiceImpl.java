package com.formation.service;

import com.formation.entities.Organisme;
import com.formation.entities.Pays;
import com.formation.response.MessageResponse;
import com.formation.repository.PaysRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PaysServiceImpl  implements PaysService {

@Autowired
PaysRepository paysRepository;

    @Transactional
    @Override
    public MessageResponse save(Pays pays) {
    	
    	// test si pays exsite déja ou non 
        boolean existe = paysRepository.existsByLibelle(pays.getLibelle());
        if (existe){
            return new MessageResponse ("Cette pays existe déja !");
        }
        paysRepository.save(pays);
        return new MessageResponse("Opération réalisée avec succès.");
    }


    @Transactional
    @Override
    public MessageResponse update(Pays pays) {
    	boolean existe = paysRepository.existsById(pays.getId());
        if (!existe){
        	boolean existe1 =paysRepository.existsByLibelle(pays.getLibelle());
            return new MessageResponse ("Cette pays existe déja !");
        }
        paysRepository.save(pays);
        return new MessageResponse("Opération réalisée avec succès.");
    }

    @Transactional
    @Override
    public MessageResponse delete(Long id) {
        Pays pays = findById(id);
        if (pays==null){
            return new MessageResponse("Cet enregistrement n'existe pas !");
        }
        paysRepository.delete(pays);
        return new MessageResponse("L'enregistrement à été supprimé avec succès.");
    }

    @Override
    public List<Pays> findAll() {

        return paysRepository.findAll();
    }

    @Override
    public Pays findById(Long id) {
        Pays pays = paysRepository.findById(id).orElse(null);
        return pays;
    }
}

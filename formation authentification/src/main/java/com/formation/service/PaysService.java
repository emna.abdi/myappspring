package com.formation.service;

import com.formation.entities.Pays;
import com.formation.response.MessageResponse;

import java.util.List;

public interface PaysService {
	
	public MessageResponse save(Pays pays); // méthode ajouter 
    public MessageResponse update( Pays pays);
    public MessageResponse delete(Long id);
    public List<Pays> findAll();
    public Pays findById(Long id);

}

package com.formation.service;

import com.formation.entities.Session_de_formation;
import com.formation.response.MessageResponse;
import com.formation.repository.Session_de_formationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class Session_formationImpl  implements Session_de_formationService {

@Autowired
Session_de_formationRepository Session_de_formationRepository;

@Override
public Session_de_formation save(Session_de_formation session) {
	// TODO Auto-generated method stub
	return Session_de_formationRepository.save(session);
}
@Transactional
@Override
public MessageResponse update(Session_de_formation session) {
	boolean existe = Session_de_formationRepository.existsById(session.getId());

    Session_de_formationRepository.save(session);
    return new MessageResponse("Opération réalisée avec succès.");
    
}
@Transactional
@Override
public void delete(Long id) {

	Session_de_formation session = findById(id);
  
    Session_de_formationRepository.delete(session);
   
}    

@Override
public List<Session_de_formation> findAll() {
	// TODO Auto-generated method stub
	return Session_de_formationRepository.findAll();
}

@Override
public Session_de_formation findById(Long id) {
	// TODO Auto-generated method stub
	Session_de_formation session= Session_de_formationRepository.findById(id).orElse(null);
    return session;
}



}


package com.formation.service;

import com.formation.entities.Profil;
import com.formation.response.MessageResponse;
import com.formation.repository.ProfilRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProfilServiceImpl implements ProfilService {

@Autowired
ProfilRepository profilRepository;

    @Transactional
    @Override
    public MessageResponse save(Profil profil) {
    	
    	// test si profil exsite déja ou non 
        boolean existe = profilRepository.existsByLibelle(profil.getLibelle());
        if (existe){
            return new MessageResponse ("Cette profil existe déja !");
        }
        profilRepository.save(profil);
        return new MessageResponse("Opération réalisée avec succès.");
    }


    @Transactional
    @Override
    public MessageResponse update(Profil profil) {
        boolean existe = profilRepository.existsById(profil.getId());
        if (!existe){
            boolean existe1 = profilRepository.existsByLibelle(profil.getLibelle());
            return new MessageResponse("Cette domaine existe déja !");

        }
        profilRepository.save(profil);
        return new MessageResponse("Opération réalisée avec succès.");
    }

    @Transactional
    @Override
    public MessageResponse delete(Long id) {
        Profil profil= findById(id);
        if (profil==null){
            return new MessageResponse("Cet enregistrement n'existe pas !");
        }
        profilRepository.delete(profil);
        return new MessageResponse("L'enregistrement à été supprimé avec succès.");
    }

    @Override
    public List<Profil> findAll() {

        return profilRepository.findAll();
    }

    @Override
    public Profil findById(Long id) {
        Profil profil = profilRepository.findById(id).orElse(null);
        return profil;
    }
}


package com.formation.service;

import com.formation.entities.Domaine;
import com.formation.entities.Formation;
import com.formation.response.MessageResponse;
import com.formation.repository.FormationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FormationServiceImpl implements FormationService{
	
	@Autowired
	FormationRepository formationRepository;
	
	@Transactional
    @Override
    public MessageResponse save(Formation formation) {
    	
		
        boolean existe = formationRepository.existsByTitre(formation.getTitre());
        if (existe){
            return new MessageResponse ("Cette formation existe déja !");
        }
        formationRepository.save(formation);
        return new MessageResponse("Opération réalisée avec succès.");
    }

	 @Transactional
	    @Override
	    public MessageResponse update(Formation formation) {
		 boolean existe = formationRepository.existsById(formation.getId());
	        if (!existe){
	        	boolean existe1 =formationRepository.existsByTitre(formation.getTitre());
	            return new MessageResponse ("Cette formation existe déja !");
	        }
	        formationRepository.save(formation);
	        return new MessageResponse("Opération réalisée avec succès.");
	    }
	 @Transactional
	    @Override
	    public MessageResponse delete(Long id) {
	        Formation formation = findById(id);
	        if (formation==null){
	            return new MessageResponse("Cet enregistrement n'existe pas !");
	        }
	        formationRepository.delete(formation);
	        return new MessageResponse("L'enregistrement à été supprimé avec succès.");
	    }

	    @Override
	    public List<Formation> findAll() {

	        return formationRepository.findAll();
	    }

	    @Override
	    public Formation findById(Long id) {
	        Formation formation = formationRepository.findById(id).orElse(null);
	        return formation;
	    }

}

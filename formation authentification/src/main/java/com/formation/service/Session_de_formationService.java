package com.formation.service;



import java.util.List;

import com.formation.entities.Session_de_formation;
import com.formation.response.MessageResponse;

public interface Session_de_formationService {
	
	public Session_de_formation save(Session_de_formation session); // méthode ajouter 
    public MessageResponse update(Session_de_formation session);
    public void delete(Long id);
    public List<Session_de_formation> findAll();
    public  Session_de_formation findById(Long id);
    
}

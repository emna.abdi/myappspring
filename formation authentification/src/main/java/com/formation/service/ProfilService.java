package com.formation.service;

import java.util.List;

import com.formation.entities.Profil;
import com.formation.response.MessageResponse;

public interface ProfilService {

	public MessageResponse save(Profil profil); // méthode ajouter 
    public MessageResponse update(Profil profil);
    public MessageResponse delete(Long id);
    public List<Profil> findAll();
    public Profil findById(Long id);
}

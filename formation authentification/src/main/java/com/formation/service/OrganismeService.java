package com.formation.service;

import com.formation.entities.Organisme;
import com.formation.response.MessageResponse;
import java.util.List;


public interface OrganismeService {
	
	public MessageResponse save(Organisme organisme); // méthode ajouter 
    public MessageResponse update( Organisme organisme);
    public MessageResponse delete(Long id);
    public List<Organisme> findAll();
    public  Organisme findById(Long IdOrganisme);

}

package com.formation.service;

import java.util.List;

import com.formation.entities.Participant;
import com.formation.response.MessageResponse;


public interface ParticipantService {

	public MessageResponse save(Participant participant); // méthode ajouter 
    public MessageResponse update(Participant participant);
    public MessageResponse delete(Long id);
    public List<Participant> findAll();
    public  Participant findById(Long id);
}

package com.formation.service;
import com.formation.entities.Formateur;
import com.formation.response.MessageResponse;
import com.formation.repository.FormateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class FormateurServiceImpl implements FormateurService {
	@Autowired
	FormateurRepository formateurRepository;
	
	@Transactional
    @Override
    public MessageResponse save(Formateur formateur) {
    	
    	// test si formateur exsite déja ou non par l'email de formation 
		
        boolean existe = formateurRepository.existsByemail(formateur.getEmail());
        if (existe){
            return new MessageResponse ("Echec :Formateur existe déja !");
        }
        formateurRepository.save(formateur);
        return new MessageResponse("Succès :Opération réalisée avec succès.");
    }

	 @Transactional
	    @Override
	    public MessageResponse update(Formateur formateur) {
		 boolean existe = formateurRepository.existsById(formateur.getId());
	        if (!existe){
	        	boolean existe1 =formateurRepository.existsByemail(formateur.getEmail());
	            return new MessageResponse ("Echec :Cette formateur existe déja !");
	        }
	        formateurRepository.save(formateur);
	        return new MessageResponse("Opération réalisée avec succès.");
	    }
	 
	 @Transactional
	    @Override
	    public MessageResponse delete(Long id) {
	        Formateur formateur = findById(id);
	        if (formateur==null){
	            return new MessageResponse("Cet enregistrement n'existe pas !");
	        }
	        formateurRepository.delete(formateur);
	        return new MessageResponse("L'enregistrement à été supprimé avec succès.");
	    }

	    @Override
	    public List<Formateur> findAll() {

	        return formateurRepository.findAll();
	    }

	    @Override
	    public Formateur findById(Long id) {
	        Formateur formateur = formateurRepository.findById(id).orElse(null);
	        return formateur;
	    }

	

}

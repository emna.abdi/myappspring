package com.formation.controllers;

import com.formation.entities.Domaine;
import com.formation.response.MessageResponse;
import com.formation.service.DomaineServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/domaines")
public class DomaineController {

    @Autowired
    private DomaineServiceImpl domaineService;

    //@RequestMapping(value="/domaines", method=RequestMethod.GET)
    
    @GetMapping
    public List<Domaine> findAll() {
        return domaineService.findAll();
    }

    @PostMapping
    public MessageResponse save(@RequestBody Domaine domaine) {
        return domaineService.save(domaine);
    }

    @PutMapping
    public MessageResponse update(@RequestBody Domaine domaine) {
        return domaineService.update(domaine);
    }

   @GetMapping("{id}")
    public Domaine findById(@PathVariable("id") Long id) {
        return domaineService.findById(id);
    }

    @DeleteMapping("{id}")
    public MessageResponse delete(@PathVariable Long id) {
        return domaineService.delete(id);
    }
}



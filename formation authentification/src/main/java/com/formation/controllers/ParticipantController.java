package com.formation.controllers;

import com.formation.entities.Participant;
import com.formation.response.MessageResponse;
import com.formation.service.ParticipantServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/participants")
public class ParticipantController {

    @Autowired
    private ParticipantServiceImpl participantService;

    //@RequestMapping(value="/participant", method=RequestMethod.GET)
    @GetMapping
    public List<Participant> findAll() {
        return participantService.findAll();
    }

    @PostMapping
    public MessageResponse save(@RequestBody Participant participant) {
        return participantService.save(participant);
    }

    @PutMapping
    public MessageResponse update(@RequestBody Participant participant) {
        return participantService.update(participant);
    }

   @GetMapping("{id}")
    public Participant findById(@PathVariable("id") Long id) {
        return participantService.findById(id);
    }

    @DeleteMapping("{id}")
    public MessageResponse delete(@PathVariable Long id) {
        return participantService.delete(id);
    }
}



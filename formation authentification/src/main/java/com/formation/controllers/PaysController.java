package com.formation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formation.entities.Pays;
import com.formation.response.MessageResponse;
import com.formation.service.PaysServiceImpl;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/pays")
public class PaysController {
	
	 @Autowired
	    private PaysServiceImpl paysService;

	    //@RequestMapping(value="/pays", method=RequestMethod.GET)
	    @GetMapping
	    public List<Pays> findAll() {
	        return paysService.findAll();
	    }

	    @PostMapping
	    public MessageResponse save(@RequestBody Pays pays) {
	        return paysService.save(pays);
	    }

	    @PutMapping
	    public MessageResponse update(@RequestBody Pays pays) {
	        return paysService.update(pays);
	    }

	   @GetMapping("{id}")
	    public Pays findById(@PathVariable("id") Long id) {
	        return paysService.findById(id);
	    }

	    @DeleteMapping("{id}")
	    public MessageResponse delete(@PathVariable Long id) {
	        return paysService.delete(id);
	    }
}

package com.formation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formation.entities.Formation;
import com.formation.response.MessageResponse;
import com.formation.service.FormationServiceImpl;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/formations")
public class FormationController {
	
	 @Autowired
	    private FormationServiceImpl formationService;

	    //@RequestMapping(value="/formation", method=RequestMethod.GET)
	    @GetMapping
	    public List<Formation> findAll() {
	        return formationService.findAll();
	    }

	    @PostMapping
	    public MessageResponse save(@RequestBody Formation formation) {
	        return formationService.save(formation);
	    }

	    @PutMapping
	    public MessageResponse update(@RequestBody Formation formation) {
	        return formationService.update(formation);
	    }

	   @GetMapping("{id}")
	    public Formation findById(@PathVariable("id") Long id) {
	        return formationService.findById(id);
	    }

	    @DeleteMapping("{id}")
	    public MessageResponse delete(@PathVariable Long id) {
	        return formationService.delete(id);
	    }
}

package com.formation.controllers;

import com.formation.entities.Organisme;
import com.formation.response.MessageResponse;
import com.formation.service.OrganismeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/organismes")
public class OrganismeController {

    @Autowired
    private OrganismeServiceImpl organismeService;

    //@RequestMapping(value="/organisme", method=RequestMethod.GET)
    @GetMapping
    public List<Organisme> findAll() {
        return organismeService.findAll();
    }

    @PostMapping
    public MessageResponse save(@RequestBody Organisme organisme) {
        return organismeService.save(organisme);
    }

    @PutMapping
    public MessageResponse update(@RequestBody Organisme organisme) {
        return organismeService.update(organisme);
    }

   @GetMapping("{id}")
    public Organisme findById(@PathVariable("id") Long id) {
        return organismeService.findById(id);
    }

    @DeleteMapping("{id}")
    public MessageResponse delete(@PathVariable Long id) {
        return organismeService.delete(id);
    }
}



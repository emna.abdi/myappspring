package com.formation.controllers;

import com.formation.entities.Profil;
import com.formation.response.MessageResponse;
import com.formation.service.ProfilServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/profils")
public class ProfilController {

    @Autowired
    private ProfilServiceImpl profilService;

    //@RequestMapping(value="/profil", method=RequestMethod.GET)
    
    @GetMapping
    public List<Profil> findAll() {
        return profilService.findAll();
    }

    @PostMapping
    public MessageResponse save(@RequestBody Profil profil) {
        return profilService.save(profil);
    }

    @PutMapping
    public MessageResponse update(@RequestBody Profil profil) {
        return profilService.update(profil);
    }

   @GetMapping("{id}")
    public Profil findById(@PathVariable("id") Long id) {
        return profilService.findById(id);
    }

    @DeleteMapping("{id}")
    public MessageResponse delete(@PathVariable Long id) {
        return profilService.delete(id);
    }
}



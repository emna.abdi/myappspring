package com.formation.controllers;

import com.formation.entities.Session_de_formation;
import com.formation.response.MessageResponse;
import com.formation.service.Session_formationImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
//@ResponseBody
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/sessions")
public class Session_de_formationController {

    @Autowired
    private Session_formationImpl sessionService;

    //@RequestMapping(value="/session", method=RequestMethod.GET)
    @GetMapping
    public List<Session_de_formation> findAll() {
        return sessionService.findAll();
    }

    @PostMapping
    public Session_de_formation save(@RequestBody Session_de_formation session) {
        return sessionService.save(session);
    }

    @PutMapping
    public MessageResponse update(@RequestBody Session_de_formation session) {
        return sessionService.update(session);
    }

   @GetMapping("{id}")
    public Session_de_formation findById(@PathVariable("id") Long id) {
        return sessionService.findById(id);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        sessionService.delete(id);
    }
}



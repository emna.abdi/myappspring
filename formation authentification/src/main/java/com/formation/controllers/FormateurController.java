package com.formation.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formation.response.MessageResponse;
import com.formation.service.FormateurServiceImpl;
import com.formation.entities.Formateur;


	//@Controller
	//@ResponseBody
	@RestController
	@CrossOrigin(origins = "http://localhost:4200")
	@RequestMapping("/api/formateurs")
	
	public class FormateurController {

	    @Autowired
	    private FormateurServiceImpl formateurService;

	    //@RequestMapping(value="/formateurs", method=RequestMethod.GET)
	    
	    @GetMapping
	    public List<Formateur> findAll() {
	        return formateurService.findAll();
	    }

	    @PostMapping
	    public MessageResponse save(@RequestBody Formateur formateur) {
	        return formateurService.save(formateur);
	    }

	    @PutMapping
	    public MessageResponse update(@RequestBody Formateur formateur) {
	        return formateurService.update(formateur);
	    }

	   @GetMapping("{id}")
	    public Formateur findById(@PathVariable("id") Long id) {
	        return formateurService.findById(id);
	    }

	    @DeleteMapping("{id}")
	    public MessageResponse delete(@PathVariable Long id) {
	        return formateurService.delete(id);
	    }
}

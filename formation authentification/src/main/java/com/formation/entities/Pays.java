package com.formation.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;


@Table
@Validated
@Entity
public class Pays {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
	private String libelle;
	
	@OneToMany (mappedBy="pays")
	private List<Participant>participants;

	
	public Pays() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Pays(String libellee, List<Participant> participants) {
		super();
		libelle = libellee;
		this.participants = participants;
	}

	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libellee) {
		libelle = libellee;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	

}

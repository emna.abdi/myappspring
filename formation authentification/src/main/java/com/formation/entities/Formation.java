package com.formation.entities;

import java.util.List;

import javax.persistence.*;
import javax.persistence.Entity;

import org.springframework.validation.annotation.Validated;

import com.formation.entities.Participant.Type;

@Table
@Entity
@Validated
public class Formation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
	private String titre; 
	private long Nb_session;
	private long Duree;
	private long Budget;
	public enum Type_form {national,international;};
	@Enumerated (EnumType.STRING)
	private Type type_form;
	@ManyToOne
	private Domaine Domaines;
	
	@OneToMany (mappedBy= "formationsss")
	   private List<Session_de_formation>formations;
    
	


	public Formation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Domaine getDomaines() {
		return Domaines;
	}
	public void setDomaines(Domaine domaines) {
		Domaines = domaines;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titree) {
		titre = titree;
	}

	
	public Type getType_form() {
		return type_form;
	}
	public void setType_form(Type type_form) {
		this.type_form = type_form;
	}



	public long getNb_session() {
		return Nb_session;
	}
	public void setNb_session(long nb_session) {
		Nb_session = nb_session;
	}
	public long getDuree() {
		return Duree;
	}
	public void setDuree(long duree) {
		Duree = duree;
	}
	

	public long getBudget() {
		return Budget;
	}
	public void setBudget(long budget) {
		Budget = budget;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	
	
	

	
	

}

package com.formation.entities;
import javax.persistence.*;




import java.util.List;

@Entity
@Table
public class Domaine {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
	@Column(unique = true,nullable = false)
	private String libelle;
	
	//Relation one To many
	@OneToMany (mappedBy="Domaines")
	private List<Formation> formations;
	

	public Domaine() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Domaine(String libelle, List<Formation> formations) {
		super();
		this.libelle = libelle;
		this.formations = formations;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	}

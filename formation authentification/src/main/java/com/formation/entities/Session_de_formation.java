package com.formation.entities;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;



@Table
@Validated
@Entity
public class Session_de_formation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
    private String Lieu;
	private Date Date_Debut;
	private Date Date_fin;
    private Long Nb_participant;
    @ManyToOne
    private Formateur formateurs;
    
    @ManyToOne
    private Organisme organismess;
    
    @ManyToOne
    private Formation formationsss;
    
    
    @ManyToMany 
    private Set<Participant> participants = new HashSet<>();
	
	
	
	public Session_de_formation() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Session_de_formation(String lieu, Date date_Debut, Date date_fin, Long nb_participant, Formateur formateurs,
			Organisme organismess, Formation formationsss, Set<Participant> participants) {
		super();
		Lieu = lieu;
		Date_Debut = date_Debut;
		Date_fin = date_fin;
		Nb_participant = nb_participant;
		this.formateurs = formateurs;
		this.organismess = organismess;
		this.formationsss = formationsss;
		this.participants = participants;
	}



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getLieu() {
		return Lieu;
	}



	public void setLieu(String lieu) {
		Lieu = lieu;
	}



	public Date getDate_Debut() {
		return Date_Debut;
	}



	public void setDate_Debut(Date date_Debut) {
		Date_Debut = date_Debut;
	}



	public Date getDate_fin() {
		return Date_fin;
	}



	public void setDate_fin(Date date_fin) {
		Date_fin = date_fin;
	}



	public Long getNb_participant() {
		return Nb_participant;
	}



	public void setNb_participant(Long nb_participant) {
		Nb_participant = nb_participant;
	}



	public Formateur getFormateurs() {
		return formateurs;
	}



	public void setFormateurs(Formateur formateurs) {
		this.formateurs = formateurs;
	}



	public Organisme getOrganismess() {
		return organismess;
	}



	public void setOrganismess(Organisme organismess) {
		this.organismess = organismess;
	}



	public Formation getFormationsss() {
		return formationsss;
	}



	public void setFormationsss(Formation formationsss) {
		this.formationsss = formationsss;
	}



	public Set<Participant> getParticipants() {
		return participants;
	}



	public void setParticipants(Set<Participant> participants) {
		this.participants = participants;
	}

    

	
    
    
    
    
}

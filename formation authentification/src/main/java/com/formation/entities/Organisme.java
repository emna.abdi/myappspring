package com.formation.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;


@Table
@Entity
@Validated

public class Organisme {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
	@Column(unique = true,nullable = false)
	private String libelle;
	
	@OneToMany (mappedBy="organismes")
	private List<Formateur>formateurs;
	
	@OneToMany(mappedBy="organismess")
	private List<Session_de_formation>sessions_formation;
	
	@OneToMany (mappedBy="organismes")
	private List<Participant>participants;
	
	
	

	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	


}

package com.formation.entities;


import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table
@Entity
public class Formateur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
    private String nom;
    private String prenom;
    private String email;
    private int tel;
    private String Type;
 
    public Formateur() {
		super();
		// TODO Auto-generated constructor stub
	}
	@ManyToOne
    private Organisme organismes;
    
   @OneToMany (mappedBy= "formateurs")
   private List<Session_de_formation>Session;
    
	
	public Organisme getOrganisme() {
	return organismes;
}
public void setOrganisme(Organisme organisme) {
	this.organismes = organisme;
}

	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Organisme getOrganismes() {
		return organismes;
	}
	public void setOrganismes(Organisme organismes) {
		this.organismes = organismes;
	}
    
}

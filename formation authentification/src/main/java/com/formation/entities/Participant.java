package com.formation.entities;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.validation.annotation.Validated;

@Table
@Validated
@Entity

public  class Participant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)  
	@Column
	private Long id;
	private String Nom;
	private String prenom;
	private String email;
	private long tel;
	
	public enum Type {national,international;};
	@Enumerated (EnumType.STRING)
	private Type type;
	
	@ManyToOne
	private Organisme organismes ;
	
	@ManyToOne
	private Pays pays;
	
	@ManyToMany(mappedBy="participants")
	private Set<Session_de_formation>sessions_formation =new HashSet<> ();
	
	@ManyToOne
	private Profil profil;
	
	
	public Participant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Participant(String nom, String prenom, String email, long tel, Type type, Organisme organismes, Pays pays,
			Set<Session_de_formation> sessions_formation, Profil profil) {
		super();
		Nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.tel = tel;
		this.type = type;
		this.organismes = organismes;
		this.pays = pays;
		this.sessions_formation = sessions_formation;
		this.profil = profil;
	}
	
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(long tel) {
		this.tel = tel;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Organisme getOrganismes() {
		return organismes;
	}
	public void setOrganismes(Organisme organismes) {
		this.organismes = organismes;
	}
	public Pays getPays() {
		return pays;
	}
	public void setPays(Pays pays) {
		this.pays = pays;
	}

	public Profil getProfil() {
		return profil;
	}
	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	
	

}

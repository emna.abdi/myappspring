import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormateurService } from '../shared/services/formateur.service';
import { Formateur } from '../shared/models/formateur.model';
import { OrganismeService } from '../shared/services/organisme.service';
import { organisme } from '../shared/models/organisme.model';

@Component({
  selector: 'app-formateur',
  templateUrl: './formateur.component.html',
  styleUrls: ['./formateur.component.css']
})
export class FormateurComponent implements OnInit {

  organismes:organisme[];
  formateurs: Formateur[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id','nom', 'prenom','email','tel','organismes','type', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<Formateur>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  profil: any;
  constructor(private formateurService: FormateurService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private organismesService :OrganismeService
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.getAllOrganismes();
    this.form = this._formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.required],
      tel: ['', Validators.required],
      type: ['', Validators.required],
      organismes: ['', Validators.required],



    });

    console.log(this.form);
  }
  getAll() {
    this.formateurService.getAll()
      .subscribe((formateur) => {
        this.formateurs = formateur.reverse();
        this.dataSource = new MatTableDataSource(this.formateurs);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(formateur);
        return formateur;

      });
  }

  getAllOrganismes(){
    this.organismesService.getAll()
      .subscribe((organismes) => this.organismes = organismes);
  }

  deleteThisformateur(id) {
    console.log(id);
    this.formateurService.deleteformateur(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateformateur(): any {
    const nom = this.form.get('nom').value;
    const prenom = this.form.get('prenom').value;
    const email = this.form.get('email').value;
    const tel = this.form.get('tel').value;
    const type = this.form.get('type').value;
    const organismes = this.form.get('organismes').value;
  const formateur = {
      nom: nom,
      prenom:prenom,
      email:email,
      tel:tel,
      type:type,
      organismes:{
        id : organismes
      }
    };


  
    console.log(formateur);
    this.formateurService.createformateur(formateur).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("Formateur ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/formateurs')];
    })

  }

  Editformateur(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.formateurService.getformateurById(id).subscribe((data: any) => {
      this.organismes= data;
      this.form.get('nom').setValue(data.nom);
      this.form.get('prenom').setValue(data.prenom);
      this.form.get('email').setValue(data.email);
      this.form.get('tel').setValue(data.tel);
      this.form.get('type').setValue(data.type);
      this.form.get('organismes').setValue(data.organismes.id);






    });

  }
  updateformateur() {
    const code = localStorage.getItem('id');
    const formateur = {
      id: code,
      nom: this.form.value.nom,
      prenom: this.form.value.prenom,
      tel: this.form.value.tel,
      email: this.form.value.email,
      type: this.form.value.type,
      organismes : {
        id : this.form.value.organismes
      }


    }
    console.log(formateur);
    this.formateurService.updateformateur(formateur).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

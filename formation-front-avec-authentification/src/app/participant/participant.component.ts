import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { organisme } from '../shared/models/organisme.model';
import { Participant } from '../shared/models/participant.model';
import { OrganismeService } from '../shared/services/organisme.service';
import { ParticipantService } from '../shared/services/participants.service';
import { Pays } from '../shared/models/pays.model';
import { Profil } from '../shared/models/profil.model';
import { PaysService } from '../shared/services/pays.service';
import { ProfilService } from '../shared/services/profil.service';

@Component({
  selector: 'app-participant',
  templateUrl: './participant.component.html',
  styleUrls: ['./participant.component.css']
})
export class ParticipantComponent implements OnInit {

  pays:Pays[];
  organisme:organisme[];
  profil:Profil[];


  participant: Participant[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = [
      'id','nom', 'prenom','email','tel','type',
      'organismes','pays','profil','modifier', 'supprimer'];
  dataSource: MatTableDataSource<Participant>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  participants: any;
    organismes: organisme[];
    Pays:Pays[];
    Profil:Profil[];
  constructor(private participantService: ParticipantService,
    private organismeService : OrganismeService,
    private paysService : PaysService,
    private profilService : ProfilService,
    private _formBuilder: FormBuilder,
    private router: Router,
    
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.getAllpays();
    this.getAllorganisme();
    this.getAllprofil();
    
    this.form = this._formBuilder.group({
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', Validators.required],
      tel: ['', Validators.required],
      type: ['', Validators.required],
      pays: ['', Validators.required],
      organismes: ['', Validators.required],
      profil: ['', Validators.required],



    });

    console.log(this.form);
  }
  getAll() {
    this.participantService.getAll()
      .subscribe((participant) => {
      
        this.participant= participant.reverse(); 
        this.dataSource = new MatTableDataSource(this.participant);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(participant);
        return participant;

      });
  }
  getAllpays(){
    this.paysService.getAll()
      .subscribe((pays) => this.pays =pays);
  }
  getAllprofil(){ 
    this.profilService.getAll() 
      .subscribe((profil) => this.profil =profil);
  }
  getAllorganisme(){
    this.organismeService.getAll()
      .subscribe((organismes) => this.organismes=organismes);
  }




  deleteThisparticipant(id) {
    console.log(id);
    this.participantService.deleteparticipant(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateparticipant(): any {
    const nom = this.form.get('nom').value;
    const prenom = this.form.get('prenom').value;
    const email = this.form.get('email').value;
    const tel = this.form.get('tel').value;
    const type = this.form.get('type').value;
    const organismes = this.form.get('organismes').value;
    const pays = this.form.get('pays').value;
    const profil = this.form.get('profil').value;

  const participant = {
      nom: nom,
      prenom:prenom,
      email:email,
      tel:tel,
      type:type,
      profil:{
        id : profil
      },
      organismes:
      {
          id:organismes
      },
      pays:
      {id:pays},
    };

    
    console.log(participant);
    this.participantService.createparticipant(participant).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("Participant ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/participants')];
    })

  }

  Editparticipant(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.participantService.getparticipantById(id).subscribe((data: any) => {
      this.participant= data;
      this.form.get('nom').setValue(data.nom);
      this.form.get('prenom').setValue(data.prenom);
      this.form.get('email').setValue(data.email);
      this.form.get('tel').setValue(data.tel);
      this.form.get('type').setValue(data.type);
      this.form.get('pays').setValue(data.pays.id);
      this.form.get('organismes').setValue(data.organismes.id);
      this.form.get('profil').setValue(data.profil.id);


    });

  }
  updateparticipant() {
    const code = localStorage.getItem('id');
    const participant = {
      id: code,
      nom: this.form.value.nom,
      prenom: this.form.value.prenom,
      email: this.form.value.email,
      tel:this.form.value.tel,
      type: this.form.value.type,
     
      pays : {
        id : this.form.value.pays
      },
      organismes : {
        id : this.form.value.organismes
      },
      profil : {
        id : this.form.value.profil
      }

    }
    console.log(participant);
    this.participantService.updateparticipant(participant).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }



}

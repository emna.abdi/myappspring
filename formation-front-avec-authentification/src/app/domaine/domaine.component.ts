import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Domaine } from 'src/app/shared/models/domaine.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DomaineService } from '../shared/services/domaine.service';

@Component({
  selector: 'app-domaine',
  templateUrl: './domaine.component.html',
  styleUrls: ['./domaine.component.css']
})
export class DomaineComponent implements OnInit {

  
  domaines: Domaine[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id', 'libelle', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<Domaine>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  domaine: any;
  constructor(private domaineService: DomaineService,
    private _formBuilder: FormBuilder,
    private router: Router,
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.form = this._formBuilder.group({
      libelle: ['', Validators.required],
    });

    console.log(this.form);
  }
  getAll() {
    this.domaineService.getAll()
      .subscribe((domaines) => {
        this.domaines = domaines.reverse();
        this.dataSource = new MatTableDataSource(this.domaines);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(domaines);
        return domaines;

      });
  }
  deleteThisdomaine(id) {
    console.log(id);
    this.domaineService.deletedomaine(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreatedomaine(): any {
    const libelle = this.form.get('libelle').value;
    const domaine = {
      libelle: libelle,
    };
    console.log(domaine);
    this.domaineService.createdomaine(domaine).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("domaine ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/domaines')];
    })

  }

  EditDomaine(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.domaineService.getdomaineById(id).subscribe((data: any) => {
      this.domaine= data;
      this.form.get('libelle').setValue(data.libelle);

    });

  }
  updatedomaine() {
    const code = localStorage.getItem('id');
    const domaine = {
      id: code,
      libelle: this.form.value.libelle,
    }
    console.log(domaine);
    this.domaineService.updatedomaine(domaine).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

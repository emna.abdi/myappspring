import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '../shared/services/session.service';
import { formation } from '../shared/models/formation.model';
import { FormateurService } from '../shared/services/formateur.service';
import { Session_de_formation } from '../shared/models/Session_de_formation.model';
import { organisme } from '../shared/models/organisme.model';
import { Participant } from '../shared/models/participant.model';
import { Session } from '@sentry/angular';
import { FormationService } from '../shared/services/formation.service';
import { Formateur } from '../shared/models/formateur.model';
import { OrganismeService } from '../shared/services/organisme.service';
import { ParticipantService } from '../shared/services/participants.service';

@Component({
  selector: 'app-formation',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.css']
})
export class Session_de_Formation implements OnInit {

  formation:formation[];
  organisme:organisme[];
  participant:Participant[];
  formateurs:Formateur[];

  session: Session_de_formation[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = [
      'id','date_Debut', 'date_fin','lieu','nb_participant','formateurs',
      'formationsss','organismess','participants','modifier', 'supprimer'];
  dataSource: MatTableDataSource<Session_de_formation>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  profil: any;
    formations: formation[];
    formateuss:Formateur[];
    participants: Participant[];
  constructor(private sessionService: SessionService,
    private formationService : FormationService,
    private formateurService : FormateurService,
    private organismeService : OrganismeService,
    private participantsService : ParticipantService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private SessionService : SessionService
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.getAllformation();
    this.getAllformateur();
    this.getAllorganisme();
    this.getAllParticipants();
    
    this.form = this._formBuilder.group({
      date_debut: ['', Validators.required],
      date_fin: ['', Validators.required],
      lieu: ['', Validators.required],
      nb_participant: ['', Validators.required],
      formateurs: ['', Validators.required],
      formations: ['', Validators.required],
      organismess: ['', Validators.required],
      participants: ['', Validators.required],



    });

    console.log(this.form);
  }
  getAll() {
    this.sessionService.getAll()
      .subscribe((session) => {
      
        this.session= session.reverse(); 
        this.dataSource = new MatTableDataSource(this.session);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(session);
        return session;

      });
  }
  getAllformation(){
    this.formationService.getAll()
      .subscribe((formationsss) => this.formations =formationsss);
  }
  getAllformateur(){ 
    this.formateurService.getAll() 
      .subscribe((formateurs) => this.formateurs =formateurs);
  }
  getAllorganisme(){
    this.organismeService.getAll()
      .subscribe((organismess) => this.organisme=organismess);
  }

  getAllParticipants(){
    this.participantsService.getAll()
      .subscribe((participants) => this.participants=participants);
  }


  deleteThissession(id) {
    console.log(id);
    this.sessionService.deletesession(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateformation(): any {
    const date_debut = this.form.get('date_debut').value;
    const date_fin = this.form.get('date_fin').value;
    const nb_participant = this.form.get('nb_participant').value;
    const lieu = this.form.get('lieu').value;
    const formateurs = this.form.get('formateurs').value;
    const organismess = this.form.get('organismess').value;
    const formationsss = this.form.get('formations').value;
    let participants = this.form.get('participants').value;
    // hedhiya tparcouri tableau [2, 3] w kol element tbadlou ma3neha 2 twalli { id : 2 }
    participants = participants.map((value) => { return { id : value } })
    console.log(participants)
  const session = {
      date_Debut: date_debut,
      date_fin:date_fin,
      nb_participant:nb_participant,
      lieu:lieu,
      formateurs:{
        id : formateurs
      },
      organismess:
      {
          id:organismess
      },
      formationsss:
      {id:formationsss},
      participants
    };

    
    console.log(session);
    this.sessionService.createsession(session).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("session ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/sessions')];
    })

  }

  Editsession(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.sessionService.getsessionById(id).subscribe((data: any) => {
      this.session= data;
      this.form.get('date_debut').setValue(data.date_Debut);
      this.form.get('date_fin').setValue(data.date_fin);
      this.form.get('lieu').setValue(data.lieu);
      this.form.get('nb_participant').setValue(data.nb_participant);
      this.form.get('formateurs').setValue(data.formateurs.id);
      this.form.get('formations').setValue(data.formationsss.id);
      this.form.get('organismess').setValue(data.organismess.id);
      this.form.get('participants').setValue(data.participants.map(p => p.id));


    });

  }
  updatesession() {
    const code = localStorage.getItem('id');
    const session = {
      id: code,
      date_Debut: this.form.value.date_debut,
      date_fin: this.form.value.date_fin,
      lieu: this.form.value.lieu,
      nb_participant: this.form.value.nb_participant,
      formateurs : {
        id : this.form.value.formateurs
      },
      formationsss : {
        id : this.form.value.formations
      },
      organismess : {
        id : this.form.value.organismess
      },
     participants:this.form.value.participants.map(id => { return { id } })



    }
    console.log(session);
    this.sessionService.updatesession(session).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

  nomDesParticipants(row : Session_de_Formation){
    return row.participants.map(p =>{return p.nom + ' ' +p.prenom}).join('\n')
  }

}

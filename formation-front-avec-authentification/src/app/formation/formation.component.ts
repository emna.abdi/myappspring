import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FormationService } from '../shared/services/formation.service';
import { formation } from '../shared/models/formation.model';
import { Domaine } from '../shared/models/domaine.model';
import { DomaineService } from '../shared/services/domaine.service';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.css']
})
export class FormationComponent implements OnInit {

  domaines:Domaine[];
  formations: formation[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id','titre', 'budget','duree','nb_session','domaines','type_form', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<formation>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  profil: any;
  constructor(private formationService: FormationService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private domainesService : DomaineService
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.getAllDomaines();
    this.form = this._formBuilder.group({
      titre: ['', Validators.required],
      budget: ['', Validators.required],
      duree: ['', Validators.required],
      nb_session: ['', Validators.required],
      type_form: ['', Validators.required],
      domaines: ['', Validators.required],



    });

    console.log(this.form);
  }
  getAll() {
    this.formationService.getAll()
      .subscribe((formations) => {
        this.formations = formations.reverse();
        this.dataSource = new MatTableDataSource(this.formations);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(formations);
        return formations;

      });
  }

  getAllDomaines(){
    this.domainesService.getAll()
      .subscribe((domaines) => this.domaines = domaines);
  }

  deleteThisformation(id) {
    console.log(id);
    this.formationService.deleteformation(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateformation(): any {
    const budget = this.form.get('budget').value;
    const nb_session = this.form.get('nb_session').value;
    const duree = this.form.get('duree').value;
    const titre = this.form.get('titre').value;
    const type_form = this.form.get('type_form').value;
    const domaines = this.form.get('domaines').value;
  const formation = {
      budget: budget,
      nb_session:nb_session,
      duree:duree,
      titre:titre,
      type_form:type_form,
      domaines:{
        id : domaines
      }
    };


  
    console.log(formation);
    this.formationService.createformation(formation).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("Formation ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/formations')];
    })

  }

  Editformation(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.formationService.getformationById(id).subscribe((data: any) => {
      this.domaines= data;
      this.form.get('budget').setValue(data.budget);
      this.form.get('duree').setValue(data.duree);
      this.form.get('nb_session').setValue(data.nb_session);
      this.form.get('type_form').setValue(data.type_form);
      this.form.get('titre').setValue(data.titre);
      this.form.get('domaines').setValue(data.domaines.id);






    });

  }
  updateformation() {
    const code = localStorage.getItem('id');
    const formation = {
      id: code,
      budget: this.form.value.budget,
      titre: this.form.value.titre,
      duree: this.form.value.duree,
      nb_session: this.form.value.nb_session,
      type_form: this.form.value.type_form,
      type: this.form.value.type,
      domaines : {
        id : this.form.value.domaines
      }


    }
    console.log(formation);
    this.formationService.updateformation(formation).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

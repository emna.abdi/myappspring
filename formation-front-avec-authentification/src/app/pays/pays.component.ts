import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Pays } from '../shared/models/pays.model';
import { PaysService } from '../shared/services/pays.service';

@Component({
  selector: 'app-pays',
  templateUrl: './pays.component.html',
  styleUrls: ['./pays.component.css']
})
export class PaysComponent implements OnInit {

  
  Pays: Pays[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id', 'libelle', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<Pays>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  pays: any;
  constructor(private paysService:PaysService,
    private _formBuilder: FormBuilder,
    private router: Router,
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.form = this._formBuilder.group({
      libelle: ['', Validators.required],
    });

    console.log(this.form);
  }
  getAll() {
    this.paysService.getAll()
      .subscribe((pays) => {
        this.Pays = pays.reverse();
        this.dataSource = new MatTableDataSource(this.Pays);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(pays);
        return pays;

      });
  }
  deleteThispays(id) {
    console.log(id);
    this.paysService.deletepays(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreatepays(): any {
    const libelle = this.form.get('libelle').value;
    const pays = {
      libelle: libelle,
    };
    console.log(pays);
    this.paysService.createpays(pays).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("Pays ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/pays')];
    })

  }

  Editpays(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.paysService.getpaysById(id).subscribe((data: any) => {
      this.Pays = data;
      this.form.get('libelle').setValue(data.libelle);

    });

  }
  updatepays() {
    const code = localStorage.getItem('id');
    const pays = {
      id: code,
      libelle: this.form.value.libelle,
    }
    console.log(pays);
    this.paysService.updatepays(pays).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

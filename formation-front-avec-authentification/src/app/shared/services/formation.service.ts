import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import { formation } from '../models/formation.model';
import { Domaine } from '../models/domaine.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FormationService {
    formations: formation[];
    domaines:Domaine[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<formation[]>(`${Config.formationUrl}/` );
    }
    getdomaine() : Observable<Domaine[]>
    {
        return this.http.get<Domaine[]>(`${Config.domaineUrl}/`);
    }
    getformationById(id) {

        return this.http.get<formation>(`${Config.formationUrl}/${id}`);
    }
    createformation(Formation): any {
        return this.http.post<formation>(`${Config.formationUrl}/`, Formation);
    }
    updateformation(Formation) {

        return this.http.put<formation>(`${Config.formationUrl}/`, Formation);
    }

    deleteformation(id) {
        return this.http.delete<formation>(`${Config.formationUrl}/${id}`);
    }




}


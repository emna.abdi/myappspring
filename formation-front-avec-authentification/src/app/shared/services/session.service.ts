import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import { Observable } from 'rxjs';
import { Session } from '@sentry/angular';
import { formation } from '../models/formation.model';
import { Formateur } from '../models/formateur.model';
import { organisme } from '../models/organisme.model';
import { Participant } from '../models/participant.model';
import { Session_de_formation } from '../models/Session_de_formation.model';

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    sessions: Session[];
    constructor(private http: HttpClient) {
    }
    
    getAll():Observable<Session_de_formation[]>{
        return this.http.get<Session_de_formation[]>(`${Config.sessionUrl}/` );
    }
    getsessionById(id) {

        return this.http.get<Session_de_formation>(`${Config.sessionUrl}/${id}`);
    }
    createsession(Session): any {
        return this.http.post<Session_de_formation>(`${Config.sessionUrl}/`, Session);
    }
    updatesession(Session) {

        return this.http.put<Session_de_formation>(`${Config.sessionUrl}/`, Session);
    }

    deletesession(id) {
        return this.http.delete<Session_de_formation>(`${Config.sessionUrl}/${id}`);
    }
    getformation() : Observable<formation[]>
    {
        return this.http.get<formation[]>(`${Config.formationUrl}/`);
    }
    getformateur() : Observable<Formateur[]>
    {
        return this.http.get<Formateur[]>(`${Config.formateurUrl}/`);
    }
    getorganisme() : Observable<organisme[]>
    {
        return this.http.get<organisme[]>(`${Config.organismeUrl}/`);
    }
    getparticipant() : Observable<Participant[]>
    {
        return this.http.get<Participant[]>(`${Config.participantUrl}/`);
    }
    



}


import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import { Formateur } from '../models/formateur.model';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class FormateurService {
    formteurs: Formateur[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<Formateur[]>(`${Config.formateurUrl}/` );
    }
    getformateur() : Observable<Formateur[]>
    {
        return this.http.get<Formateur[]>(`${Config.formateurUrl}/`);
    }
    getformateurById(id) {

        return this.http.get<Formateur>(`${Config.formateurUrl}/${id}`);
    }
    createformateur(Formateur): any {
        return this.http.post<Formateur>(`${Config.formateurUrl}/`, Formateur);
    }
    updateformateur(Formateur) {

        return this.http.put<Formateur>(`${Config.formateurUrl}/`, Formateur);
    }

    deleteformateur(id) {
        return this.http.delete<Formateur>(`${Config.formateurUrl}/${id}`);
    }




}


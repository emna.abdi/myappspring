import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import {Pays} from '../models/pays.model';


@Injectable({
    providedIn: 'root'
})
export class PaysService {
    Pays: Pays[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<Pays[]>(`${Config.paysUrl}/` );
    }
    getpaysById(id) {

        return this.http.get<Pays>(`${Config.paysUrl}/${id}`);
    }
    createpays(pays): any {
        return this.http.post<Pays>(`${Config.paysUrl}/`,pays);
    }
    updatepays(pays) {

        return this.http.put<Pays>(`${Config.paysUrl}/`,pays);
    }

    deletepays(id) {
        return this.http.delete<Pays>(`${Config.paysUrl}/${id}`);
    }




}


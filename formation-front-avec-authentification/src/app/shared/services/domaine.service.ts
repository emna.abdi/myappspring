import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Domaine } from '../models/domaine.model';
import {Config} from '../config/config'
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DomaineService {
    Domaines: Domaine[];
    constructor(private http: HttpClient) {
    }
    
    getAll():Observable<Domaine[]>{

        return this.http.get<Domaine[]>(`${Config.domaineUrl}/` );
    }
    getdomaineById(id) {

        return this.http.get<Domaine>(`${Config.domaineUrl}/${id}`);
    }
    createdomaine(Domaine): any {
        return this.http.post<Domaine>(`${Config.domaineUrl}/`, Domaine);
    }
    updatedomaine(Domaine) {

        return this.http.put<Domaine>(`${Config.domaineUrl}/`, Domaine);
    }

    deletedomaine(id) {
        return this.http.delete<Domaine>(`${Config.domaineUrl}/${id}`);
    }




}


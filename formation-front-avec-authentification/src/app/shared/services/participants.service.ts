import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import { Participant } from '../models/participant.model';

@Injectable({
    providedIn: 'root'
})
export class ParticipantService {
    Participant: Participant[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<Participant[]>(`${Config.participantUrl}/` );
    }
    getparticipantById(id) {

        return this.http.get<Participant>(`${Config.participantUrl}/${id}`);
    }
    createparticipant(participant): any {
        return this.http.post<Participant>(`${Config.participantUrl}/`, participant);
    }
    updateparticipant(participant) {

        return this.http.put<Participant>(`${Config.participantUrl}/`, participant);
    }

    deleteparticipant(id) {
        return this.http.delete<Participant>(`${Config.participantUrl}/${id}`);
    }




}


import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { organisme } from '../models/organisme.model';
import {Config} from '../config/config'

@Injectable({
    providedIn: 'root'
})
export class OrganismeService {
    organisme: organisme[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<organisme[]>(`${Config.organismeUrl}/` );
    }
    getorganismeById(id) {

        return this.http.get<organisme>(`${Config.organismeUrl}/${id}`);
    }
    createorganisme(organisme): any {
        return this.http.post<organisme>(`${Config.organismeUrl}/`, organisme);
    }
    updateorganisme(organisme) {

        return this.http.put<organisme>(`${Config.organismeUrl}/`, organisme);
    }

    deleteorganisme(id) {
        return this.http.delete<organisme>(`${Config.organismeUrl}/${id}`);
    }




}


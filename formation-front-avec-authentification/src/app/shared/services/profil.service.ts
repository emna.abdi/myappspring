import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import {Config} from '../config/config'
import { Profil } from '../models/profil.model';

@Injectable({
    providedIn: 'root'
})
export class ProfilService {
    Profils: Profil[];
    constructor(private http: HttpClient) {
    }
    
    getAll() {

        return this.http.get<Profil[]>(`${Config.profilUrl}/` );
    }
    getprofilById(id) {

        return this.http.get<Profil>(`${Config.profilUrl}/${id}`);
    }
    createprofil(Profil): any {
        return this.http.post<Profil>(`${Config.profilUrl}/`, Profil);
    }
    updateprofil(Profil) {

        return this.http.put<Profil>(`${Config.profilUrl}/`, Profil);
    }

    deleteprofil(id) {
        return this.http.delete<Profil>(`${Config.profilUrl}/${id}`);
    }




}


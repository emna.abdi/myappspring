import { organisme } from "./organisme.model";
import {Typeform} from "./Typeform"
export class Formateur 
{
    id:number;
    nom:String;
    prenom:String;
    email:String;
    tel:number;
    type:Typeform;
    organisme:organisme;
}
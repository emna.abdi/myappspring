import { Formateur } from "./formateur.model";
import { formation } from "./formation.model";
import { organisme } from "./organisme.model";
import { Participant } from "./participant.model";

export class Session_de_formation {
    id : number ;
   lieu:String;
   date_debut:Date;
   date_fin:Date;
   nb_participant:number;
   formateurs:Formateur;
   formationsss:formation;
   organismess:organisme;
   participants:Participant;


}
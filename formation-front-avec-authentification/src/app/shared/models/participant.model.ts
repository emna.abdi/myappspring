import { Session } from "@sentry/types";
import { organisme } from "./organisme.model";
import { Pays } from "./pays.model";
import { Profil } from "./profil.model";
import { Type } from "./Type";

export class Participant {
    id : number ;
   nom:String;
   prenom:String;
   email:string;
   tel:number;
   type:Type;
   organismes:organisme;
   pays:Pays;
   profil:Profil;
   sessions_formation:Session;
}
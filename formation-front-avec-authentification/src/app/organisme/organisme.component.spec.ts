import { ComponentFixture, TestBed } from '@angular/core/testing';

import {  OrgnismeComponent } from './organisme.component';

describe('OrgnismeComponent', () => {
  let component:  OrgnismeComponent;
  let fixture: ComponentFixture< OrgnismeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrgnismeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent( OrgnismeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

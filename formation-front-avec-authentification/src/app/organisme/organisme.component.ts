import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { OrganismeService } from '../shared/services/organisme.service';
import { organisme } from '../shared/models/organisme.model';

@Component({
  selector: 'app-organisme',
  templateUrl: './organisme.component.html',
  styleUrls: ['./organisme.component.css']
})
export class OrgnismeComponent implements OnInit {

  
  Organisme: organisme[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id', 'libelle', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<organisme>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  organisme: any;
  constructor(private organismeService:OrganismeService,
    private _formBuilder: FormBuilder,
    private router: Router,
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.form = this._formBuilder.group({
      libelle: ['', Validators.required],
    });

    console.log(this.form);
  }
  getAll() {
    this.organismeService.getAll()
      .subscribe((organismes) => {
        this.organisme = organismes.reverse();
        this.dataSource = new MatTableDataSource(this.organisme);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(organismes);
        return organismes;

      });
  }
  deleteThisorganisme(id) {
    console.log(id);
    this.organismeService.deleteorganisme(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateorganisme(): any {
    const libelle = this.form.get('libelle').value;
    const organisme = {
      libelle: libelle,
    };
    console.log(organisme);
    this.organismeService.createorganisme(organisme).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("organisme ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/organismes')];
    })

  }

  Editorganisme(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.organismeService.getorganismeById(id).subscribe((data: any) => {
      this.organisme = data;
      this.form.get('libelle').setValue(data.libelle);

    });

  }
  updateorganisme() {
    const code = localStorage.getItem('id');
    const organisme = {
      id: code,
      libelle: this.form.value.libelle,
    }
    console.log(organisme);
    this.organismeService.updateorganisme(organisme).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

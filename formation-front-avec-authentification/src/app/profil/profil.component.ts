import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Domaine } from 'src/app/shared/models/domaine.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfilService } from '../shared/services/profil.service';
import { Profil } from '../shared/models/profil.model';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  
  profils: Profil[];
  form: FormGroup;
  btnUpdateShow:boolean = false;
  btnSaveShow:boolean = true;
  @ViewChild('closebutton') closebutton;
  displayedColumns: string[] = ['id', 'libelle', 'modifier', 'supprimer'];
  dataSource: MatTableDataSource<Profil>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  profil: any;
  constructor(private profilService: ProfilService,
    private _formBuilder: FormBuilder,
    private router: Router,
) {
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.getAll();
    this.form = this._formBuilder.group({
      libelle: ['', Validators.required],
    });

    console.log(this.form);
  }
  getAll() {
    this.profilService.getAll()
      .subscribe((profils) => {
        this.profils = profils.reverse();
        this.dataSource = new MatTableDataSource(this.profils);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        console.log(profils);
        return profils;

      });
  }
  deleteThisprofil(id) {
    console.log(id);
    this.profilService.deleteprofil(id).subscribe({
      next: (res) => {
        this.getAll();
      },
    });

  }

  OnCreateprofil(): any {
    const libelle = this.form.get('libelle').value;
    const profil = {
      libelle: libelle,
    };
    console.log(profil);
    this.profilService.createprofil(profil).subscribe(res =>{
      this.closebutton.nativeElement.click();
      alert("profil ajouté avec succès");
      this.getAll();
      this.form.reset();
      this.router.navigateByUrl[('/profils')];
    })

  }

  Editprofil(id) {
    this.UpdateShowBtn();
    localStorage.setItem('id', id);
    this.profilService.getprofilById(id).subscribe((data: any) => {
      this.profil= data;
      this.form.get('libelle').setValue(data.libelle);

    });

  }
  updateprofil() {
    const code = localStorage.getItem('id');
    const profil = {
      id: code,
      libelle: this.form.value.libelle,
    }
    console.log(profil);
    this.profilService.updateprofil(profil).subscribe(res =>{
      console.log(res);
      this.closebutton.nativeElement.click();
      this.SaveShowBtn();
      this.getAll();
      
    })

  }
  UpdateShowBtn()
  {
    this.btnUpdateShow = true;
    this.btnSaveShow = false;
  }
  SaveShowBtn()
  {
    this.btnUpdateShow = false;
    this.btnSaveShow = true;
  }

}

import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MatMenuModule} from '@angular/material/menu';
import { DomaineComponent } from './domaine/domaine.component';
import { OrgnismeComponent } from './organisme/organisme.component';
import { ProfilComponent } from './profil/profil.component';
import { PaysComponent } from './pays/pays.component';
import { FormationComponent } from './formation/formation.component';
import { Session_de_Formation } from './session/session.component';
import { FormateurComponent } from './formateur/formateur.component';
import { ParticipantComponent } from './participant/participant.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [

  {path: 'domaines',component: DomaineComponent,},
  {
    path: 'organismes',
    component: OrgnismeComponent,
    
  },
  {
    path: 'profils',
    component: ProfilComponent,
   
  },
  {
    path: 'pays',
    component: PaysComponent,
  },
  {
    path: 'formations',
    component: FormationComponent,
   
  },
  {
    path: 'sessions',
    component: Session_de_Formation,
    
  },

  {
    path: 'formateurs',
    component: FormateurComponent,
   
  },
  {
    path: 'participants',
    component: ParticipantComponent,
  
  },
  

  {
    path: 'register',
    component: RegisterComponent,
  
  },
  {
    path: 'login',
    component: LoginComponent,
  
  },
  {
    path: 'dashboard',
    component: StudentDashboardComponent,
  
  },
  {
    path: 'home',
    component: HomeComponent,
  
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'login/register',
    component: RegisterComponent,
  
  },

  {
    path: 'login/home',
    component: HomeComponent,
  
  },
  {
    path: 'login/register/login',
    component: LoginComponent,
  
  },
  {
    path: 'register/home',
    component: HomeComponent,
  
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
